# **Octave Excersises**
-----------------------------------------------------------
## Requerimientos
> **GNU Octave**

> **Python**

> **Latex**

> Editor con soporte a Latex, Python, Octave.

## **Instalación**

#### *Linux*
``` $ sudo apt-get install octave```


#### *OS X 10.6 o Superior*

> - Requerimiento: **Howebrew** desde [aquí][1]

Comprobar actualizaciones de package en brew:
``` $ brew update && upgrade```

Instalar [SCIENCE][3], repositorio que contiene los instaladores brew para software para especializados en las ciencias exactas:
``` $ brew tap homebrew/science```

Instalar octave
``` $ brew install octave```

> **Notas:**
        verificar dependencias y librerías:
        ``` $ brew doctor```

Instalar [XQUARTZ][4] Terminal

#### *Windows*

> - Requerimiento: **octave-4.0.1-installer.exe** desde [aquí][2]


  [1]: http://brew.sh/
  [2]: ftp://ftp.gnu.org/gnu/octave/windows/octave-4.0.1-installer.exe
  [3]: https://github.com/Homebrew/homebrew-science
  [4]: http://www.xquartz.org/
